<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Template {

	public $template = 'site';

	public function action_index()
	{
		$main = ORM::factory('Main');
		$m = $main->find_all();
	
		$this->template->list = $m;
	}

} // End Main
