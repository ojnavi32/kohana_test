<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api extends Controller {

	public function action_index()
	{
		$params = $this->request->post();
		$params['date_added'] = date('F j, Y');

		if (isset($_FILES['filename'])) {
			// $filename = $this->_save_image($_FILES['filename']);

			$directory = DOCROOT.'media/uploads/';
		
			if ($file = Upload::save($_FILES['filename'], NULL, $directory))
			{
				$filename = $params['thumbnail'].'.jpg';
				
				Image::factory($file)
					->resize(200, 200, Image::AUTO)
					->save($directory.$filename);

				// Delete the temporary file
				unlink($file);
			}
		}

		$main = ORM::factory('Main');

		$main->title = $params['title'];
		$main->thumbnail = $filename;
		$main->filename = $_FILES['filename']['name'];
		$main->date_added = strtotime('today');
		$main->save();

		$response = [
			'response' => ['status' => 'Success!', 'message' => 'Successfully Added!'],
			'data' => $params,
		];
		echo json_encode($response);
		// $this->response->body('templates/layouts');
		// // $this->load->view('templates/layouts', $title);
	}

	public function action_list(  )
	{
		$main = ORM::factory('Main');
		$m = $main->find_all();
		foreach ($m as $val) {
			$_m[] = [
				'id' => $val->id,
				'title' => $val->title,
				'thumbnail' => '<img src="media\uploads/'.$val->thumbnail.'" width="150" height="50">',
				'filename' => $val->filename,
				'date_added' => date('F j, Y', $val->date_added),
				'action' => '<button class="btn btn-warning btn-xs" data-id="'.$val->id.'" title="Edit"><i class="fa fa-pencil"></i></button> | <button class="btn btn-danger btn-xs" data-id="'.$val->id.'" title="Delete"><i class="fa fa-trash"></i></button>',
				];
		}

		if (empty($_m)) {
			$data = [];
		} else {
			$data = [
				'data' => $_m,
			];
		}

	echo json_encode($data);
	}

	public function action_edit(  )
	{
		$params = $this->request->param();
		$id = $params['id'];

		$main = ORM::factory('Main');
		$m = $main->where('id', '=', $id)->find_all();

		foreach ($m as $val) {
			$data = [
				'id' => $val->id,
				'title' => $val->title,
				'thumbnail' => $val->thumbnail,
				'filename' => $val->filename,
			];
		}
		echo json_encode($data);
	}

	public function action_update(  )
	{
		$params = $this->request->post();

		if (isset($_FILES['filename'])) {
			// $filename = $this->_save_image($_FILES['filename']);

			$directory = DOCROOT.'media/uploads/';
		
			if ($file = Upload::save($_FILES['filename'], NULL, $directory))
			{
				$filename = $params['thumbnail'].'.jpg';
				
				Image::factory($file)
					->resize(200, 200, Image::AUTO)
					->save($directory.$filename);

				// Delete the temporary file
				unlink($file);
			}
		}

		$main = ORM::factory('Main', $params['id']);
		$main->title = $params['title'];
		$main->thumbnail = $filename;
		$main->filename = $_FILES['filename']['name'];
		$main->date_added = strtotime('today');
		$main->save();

		$response = [
			'response' => ['status' => 'Success!', 'message' => 'Successfully Updated!'],
			'data' => $params,
		];
		echo json_encode($response);
	}

	public function action_delete(  )
	{
		$params = $this->request->param();
		$id = $params['id'];

		ORM::factory('Main', $id)->delete();

		echo json_encode(['status' => 'Deleted!', 'message' => 'Successfully Deleted!']);
	}

	protected function _save_image($image)
	{
		if (
			! Upload::valid($image) OR
			! Upload::not_empty($image) OR
			! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
		{
			return FALSE;
		}
		
		$directory = DOCROOT.'media/uploads/';
		
		if ($file = Upload::save($image, NULL, $directory))
		{
			$filename = strtolower(Text::random('alnum', 20)).'.jpg';
			
			Image::factory($file)
				->resize(200, 200, Image::AUTO)
				->save($directory.$filename);

			// Delete the temporary file
			unlink($file);
			
			return $filename;
		}
		
		return FALSE;
	}

} // End Welcome
