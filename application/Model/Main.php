<?php defined('SYSPATH') or die('No direct script access.');

class Model_Main extends ORM {

	public function rules()
	{
		return array(
			'title' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 255)),
				array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
			),
			'thumbnail' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 255)),
				array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
			),
			'filename' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 255)),
				array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
			),
			'date_added' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 255)),
				array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
			),
		);
	}
}

?>