<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="media/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Datatables -->
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
  <!-- Sweetalert -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="media/css/theme.min.css">
  <!-- Own Style -->
  <link rel="stylesheet" href="media/css/style.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Ad</b>MIN</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="media/img/avatar04.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Ivanjo Sarmiento</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="media/img/avatar04.png" class="img-circle" alt="User Image">

                <p>
                  Ivanjo Sarmiento
                  <small>Full Stack Web Developer</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 1.0.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
	          <div class="box">
	            <div class="box-header with-border">
	              <h3 class="box-title">List</h3>

	              <div class="box-tools pull-right">
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	              </div>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            	<button class="btn btn-success btn-lg btn-add-new-list" data-toggle="modal" data-target="#addNew">Add New</button>
	            	<table id="list-table" class="table table-striped table-hover">
	            		<thead>
	            			<th>TITLE</th>
	            			<th>THUMBNAIL</th>
	            			<th>FILENAME</th>
	            			<th>DATE ADDED</th>
                    <th>ACTION</th>
	            		</thead>
<!-- 	            		<tbody>
	            			<?php foreach ($list as $val) { ?>
	            			<tr>
	            				<td><?= $val->title ?></td>
	            				<td><?= $val->thumbnail ?></td>
	            				<td><?= $val->filename ?></td>
	            				<td><?= date('F j, Y', $val->date_added) ?></td>
	            				<td>
	            					<button class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>
	            					<button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
	            					</td>
	            			</tr>
	            			<?php } ?>
	            		</tbody> -->
 	            	</table>
	            </div>
	        </div>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?= date('Y') ?> <a href="#">Ivanjo T. Sarmiento</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- Modal Addnew -->
<div class="modal fade" id="addNew" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add New</h4>
    </div>

    <form id="idForm" enctype="multipart/form-data">
    <div class="modal-body">
		<div class="form-group">
			<label>Title:</label>
			<input type="text" class="form-control f_title" name="title" required="">
		</div>
		<div class="form-group">
			<label>Thumbnail:</label>
			<input type="text" class="form-control f_thumbnail" name="thumbnail" required="">
		</div>
		<div class="form-group">
			<label class="">Filename:</label>
			<input type="file" class="form-control f_filename" name="filename">
      <!-- <input type="hidden" class="f_filename_value" name="filename"> -->
		</div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-default add-new-list">Submit</button>
    </div>
    </form>
  </div>
  
</div>
</div>

<!-- Modal Addnew -->
<div class="modal fade" id="editList" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Edit List</h4>
    </div>
    
    <form id="idFormEdit" enctype="multipart/form-data">
    <div class="modal-body">
    <input type="hidden" class="fe_id" name="id">
    <div class="form-group">
      <label>Title:</label>
      <input type="text" class="form-control fe_title" name="title" required="">
    </div>
    <div class="form-group">
      <label>Thumbnail:</label>
      <input type="text" class="form-control fe_thumbnail" name="thumbnail" required="">
    </div>
    <div class="form-group">
      <label>Filename:</label>
      <input type="file" class="form-control fe_filename" name="filename">
      <img src="" alt="" class="fe_img" width="150" height="50">
    </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-success edit-list-confirm">Submit</button>
    </div>
    </form>
  </div>

</div>
</div>

<!-- jQuery 2.2.3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="media/js/bootstrap.min.js"></script>
<!-- Datatables -->
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<!-- Sweetalert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- AdminLTE App -->
<script src="media/js/app.min.js"></script>

<!-- Custom Scripts -->
<script>
	$(function() {

    var table = $('#list-table').DataTable({
      'ajax': '/kohana/api/list',
      'deferRender': true,
      'columns': [
        { 'data': 'title' },
        { 'data': 'thumbnail' },
        { 'data': 'filename' },
        { 'data': 'date_added' },
        { 'data': 'action' },
      ],
      fnDrawCallback: function (settings) {
          $("#list-table").parent().toggle(settings.fnRecordsDisplay() > 0);
      }
    });

    // Reset Add New Modal
    $('.btn-add-new-list').click(function() {
     $('.f_title').val('');
     $('.f_thumbnail').val('');
     $('.f_filename').val('');
      $('.modal-title').text('Add New');
    });

		$('#idForm').on('submit', function(e) {
      e.preventDefault();
			var form = $('#idForm');
      var data = new FormData($(this));
      console.log(data);
      // var filename = $('.f_filename').val().replace(/C:\\fakepath\\/i, '');
      // $('.f_filename_value').val(filename);
			$.ajax({
				type: 'POST',
				url: '/kohana/api',
				data: new FormData(this),
        processData: false,
        contentType: false,
				success: function(data) {
          console.log(data);
					var json = $.parseJSON(data);
          $('#addNew').modal('hide');
					// // Sweet alert
					swal(json.response.status, json.response.message);
          $('#list-table').DataTable().ajax.reload();
				}
			});
		});

    $('#list-table tbody').on( 'click', '.btn-warning', function () {
          var id = $(this).attr('data-id');

          $.get('/kohana/api/edit/'+id, function(data) {
            var json = $.parseJSON(data);
            $('.fe_id').val(json.id);
            $('.fe_title').val(json.title);
            $('.fe_thumbnail').val(json.thumbnail);
            $('.fe_img').attr('src', 'media/uploads/'+json.thumbnail)
            // $('.fe_filename').val(json.filename);

            $('.modal-title').text('Edit List '+json.title);
            $('#editList').modal('show');
          });
    } );

    $('#idFormEdit').on('submit', function(e) {
      e.preventDefault();
      $('.fe_filename').required;
      $.ajax({
        type: 'POST',
        url: '/kohana/api/update',
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
          console.log(data);
          // $('#list-table > tbody:last').append('<tr></tr>');
          var json = $.parseJSON(data);
          $('#editList').modal('hide');
          // Sweet alert
          swal(json.response.status, json.response.message);
          $('#list-table').DataTable().ajax.reload();

        }
      });
    });

    $('#list-table tbody').on( 'click', '.btn-danger', function () {
      var id = $(this).attr('data-id');

      swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        $.get('/kohana/api/delete/'+id, function(data) {
            var json = $.parseJSON(data);

            swal(json.status, json.message, "success");
            $('#list-table').DataTable().ajax.reload();
        });
      });

    });
	});
</script>
</body>
</html>
